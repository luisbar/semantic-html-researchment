This repo contains useful examples and info about semantic HTML and SEO

## Foundation
- HTML: it is the World Wide Web's core markup language. Originally, HTML was primarily designed as a language for semantically describing scientific documents. Its general design, however, has enabled it to be adapted, over the subsequent years, to describe a number of other types of documents and even applications.
  - HTML5: the term "HTML5" is widely used as a buzzword to refer to modern web technologies, many of which (though by no means all) are developed at the WHATWG.
  - [WHATWG](https://whatwg.org): Web Hypertext Application Technology Working Group and the World Wide Web Consortium (W3C) work together in order to develop a single version of [the HTML and DOM specification](https://html.spec.whatwg.org)
- Node: a `node` is the generic name for any type of object in the DOM hierarchy. A `node` could be one of the built-in DOM elements such as `document` or `document.body`, it could be an HTML tag specified in the HTML such as `<input>` or `<p>` or it could be a text node that is created by the system to hold a block of text inside another element. So, in a nutshell, a node is any DOM object.
- Element: an element is one specific type of node such as `<input>`, `<span>`, `<img>`, etc.
  - The HTML element is everything from the start tag to the end tag:
  ```html
  <tagname>Content goes here...</tagname>
  ```
  - [Here](https://html.spec.whatwg.org/#toc-semantics) you can find the element grouped by category
  - HTML elements has attributes, for instance
  ```html
  <img src="image.png">
  ```
  - Link element is most commonly known for communicating relationships between pages to search engines
## HTML elements
- [Doctype](https://html.spec.whatwg.org/#the-doctype): it is required for legacy reasons. When omitted, browsers tend to use a different rendering mode that is incompatible with some specifications. Including the DOCTYPE in a document ensures that the browser makes a best-effort attempt at following the relevant specifications
- [html](https://html.spec.whatwg.org/#the-root-element): The html element represents the root of an HTML document.
  - Authors are encouraged to specify a lang attribute on the root html element, giving the document's language. This aids speech synthesis tools to determine what pronunciations to use, translation tools to determine what rules to use, and so forth
- [head](https://html.spec.whatwg.org/#document-metadata): this element represents a collection of metadata for the Document. 
  - Charset meta define the codification of the website
  ```html
  <meta charset="UTF-8">
  ```
  - Viewport meta works for redimension the viewport taking the device width
  ```html
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  ```
  - The robots meta works for improving the SEO or to avoid Google crawlers index your web site
    - A Web crawler, sometimes called a spider or spiderbot and often shortened to crawler, is an Internet Bot that systematically browses the internet and that is typically operated by search engines for the purpose of Web Indexing (web spidering)
      - Common Crawl has an open source repo https://commoncrawl.org
    - Robot.txt files tell search engine crawlers which URLs the crawler can access on your site. The is used mainly to avoid overloading your site with requests, it is not a mechanism for keeping a web page out of Google
    - In general terms, if you want to deindex a page or directory from Google’s Search Results then we suggest that you use a “Noindex” meta tag rather than a robots.txt directive as by using this method the next time your site is crawled your page will be deindexed, meaning that you won’t have to send a URL removal request. However, you can still use a robots.txt directive coupled with a Webmaster Tools page removal to accomplish this.
    - Using a meta robots tag also ensures that your **link equity** is not being lost, with the use of the ‘follow’ command.
    - Robots.txt files are best for disallowing a whole section of a site, such as a category whereas a meta tag is more efficient at disallowing single files and pages. You could choose to use both a meta robots tag and a robots.txt file as neither has authority over the other, but “noindex” always has authority over “index” requests.
  - The theme-color is nice for changing the color of some browsers, because there are some metas are supporting only for certain browsers
  ```html
  <meta name="theme-color" content="#987AEF">
  ```
  ![Theme color](/screenshots/theme-color.png)
  - The favicon meta is for adding an icon
    - You can use [this generator](https://favicon.io)
  ```html
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
  ```
  ![Favicon](/screenshots/favicon.png)
  - The manifest meta it is for progressive web applications (PWA)
  ```html
  <link rel="manifest" href="/favicon/site.webmanifest">
  ```
  - [Opengraph](https://ogp.me) metas are for displaying correctly page content on different social networks, it is a standard created by Facebook.
    - You can use [opengraph.xyz](https://www.opengraph.xyz) in order to validate if you have the right tags
  - The ‘rel alternate’ tag is similar to the canonical tag, but unlike the canonical tag, the alternate tag can be used to establish a relationship between pages that have an entirely different version of the ‘same’ content.
    - For example, pages that have the same content but written in different languages or pages that have the same content but are made to display on a different media devices, like desktop and mobile pages.
    - For example, say you have an English version of a webpage which has been translated into German. The links to the English and German version are as follows:
    
      English version: http://example.com/some-article/
      
      German version: http://example.com/some-article-german/
    
    - In this case, you can add the following tag to the English version of the webpage to indicate to Google that a German version of this page is present at the linked URL, as follows:
    ```html
    <link rel=”alternate” href=”http://example.com/some-article-german/” hreflang=”de” />
    ```
    - The `hreflang` attribute indicates the language of the linked page and `de` is the language code for German.
  
    - If you have an entire translated website, you can add the rel alternate tag at a domain level like this:
    ```html
    <link rel=”alternate” href=”http://example.com/de/” hreflang=”de” />
    ```
  - To put it simply, a canonical tag tells Google and other search engine bots that two or more webpages are essentially the same (in terms of content) but one is a variation or duplicate of the main page represented in the canonical tag.

    - As an example, take a look at the following URLs:
  
      http://example.com/article-on-veggies/

      https://example.com/article-on-veggies/

      https://www.example.com/article-on-veggies/

      http://example.com/article-on-veggies/comments/

      http://example.com/article-on-veggies/1/
  
    - As you can see, all the above pages have the same content, but different URLs.
  
    - When the site is not configured properly, you’d be able to visit the same article using all these URLs, but because the content is exactly the same, when Google indexes all these pages, it detects duplicate content.
  
    - And this is what happens when Google detects duplicate content: 
  
    >In the rare cases in which Google perceives that duplicate content may be shown with intent to manipulate our rankings and deceive our users, we’ll also make appropriate adjustments in the indexing and ranking of the sites involved. As a result, the ranking of the site may suffer, or the site might be removed entirely from the Google index, in which case it will no longer appear in search results.
  
    - So how do you avoid this issue? This is where the ‘canonical tag’ comes in. By adding a canonical tag to these pages, you can easily inform Google (and other search engines) that all these pages are mere variations of a single, original page.
  
    - Continuing with our example, all the above pages should have the following canonical tag:
    ```html
    <link rel=”canonical” href=”https://example.com/article-on-veggies/” />
    ```
  
    - Using this canonical tag tells Google that all the other pages are copies of the main page which in this case is: https://example.com/article-on-veggies/
## Hints and tricks
- You can find all info related to HTML [here](https://html.spec.whatwg.org)
- [SonarLint](https://www.sonarsource.com/products/sonarlint/?gads_campaign=SL-Mroi-Brand&gads_ad_group=SonarLint&gads_keyword=sonarlint&gclid=CjwKCAjw-KipBhBtEiwAWjgwrE18JdTM85gHTUbJkod_L9-m1nUbVyeY1ssQcThjaM7Hs75BjUZfGRoCpgsQAvD_BwE) extension could help you to solve HTML issues
- Image element has the `loading` attribute which allows you to load images either in eager or lazy mode
- If you want to adapt an image the width of your site you have to use the `aspect-ratio` style
  ```html
  <img
    src="../public/me.jpeg"
    alt="Picture of Angel Barrancos"
    style="width: 100%; aspect-ratio: 1/1;"
  >
  ```
- You can get the aspect-ration using the dev tools
  ![Aspect ratio](/screenshots/aspect-ratio.png)